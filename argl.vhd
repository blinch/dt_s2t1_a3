-- architecture argl - operator overloading - for 11bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_unsigned.all;
    
architecture argl of counter is
    -- 001 aril
    -- 010 argl
    -- 100 arop    
    signal did_s     : std_logic_vector(  2 downto 0) := "010";
    signal err_ns    : std_logic;
    signal err_cs    : std_logic := '0';
    signal count_ns  : std_logic_vector( 11 downto 0);
    signal count_cs  : std_logic_vector( 11 downto 0) := (others=>'0');
    --
    signal valLd_ns  : std_logic_vector( 11 downto 0);
    signal valLd_cs  : std_logic_vector( 11 downto 0) := (others=>'0');
    signal nLd_ns    : std_logic;
    signal nLd_cs    : std_logic := '0';
    signal updown_ns : std_logic_vector ( 1 downto 0);
    signal updown_cs : std_logic_vector ( 1 downto 0) := "00";
    --
    signal operand_s : std_logic_vector (11 downto 0);
    signal carry_s   : std_logic_vector (12 downto 0);
	
	signal result_s  : std_logic_vector (11 downto 0);

    component fullAdd is
        port(
            bitA      : in  std_logic;
            bitB      : in  std_logic;
            carry_in  : in  std_logic;
            resultBit : out std_logic;
            carry_out : out std_logic
        );
    end component fullAdd;
    for all : fullAdd use entity work.fullAdd(arc);
    
begin

	carry_s(0) <= '0';  -- carry in an bit position 0 dauerhaft auf GND legen
	
	
    combiLogic:
    process (updown_cs, count_cs, nLd_cs, valLd_cs, carry_s, err_cs, result_s) is 
        variable result_v  : std_logic_vector(11 downto 0);
        variable operand_v : std_logic_vector(11 downto 0);
        variable updown_v  : std_logic_vector( 1 downto 0);
        variable err_v     : std_logic;
    begin
        err_v := err_cs;
        operand_v := (others => '0');
		updown_v := updown_cs;
        
        if(nLd_cs = '0') then       -- load new count value
            result_v := valLd_cs;
			err_v := '0';
        else                        -- count:
			result_v := result_s;
            case updown_v is
                when "01" =>        -- down
                    operand_v := (others => '1');
                when "10" =>        -- up
                    operand_v    := (others => '0');
                    operand_v(0) := '1';
                when others =>      -- do nothing
                    null;
            end case;
            ----------------
            err_v := err_v or (carry_s(12) xor updown_cs(0)); -- updown_cs(0) --> down bit
            -- nothing more to do here, because all the computation happens in the bitwise full adder
            -- full adders are constructed mainly in a generate loop (see all the port mappings down below)
            ----------------
        end if;
        count_ns  <= result_v;
        operand_s <= operand_v;
        err_ns    <= err_v;
        
    end process combiLogic;
    
    
    -- carry_s(0) is already set to '0'
    generateLoopFA:
    for i in 0 to 11 generate
    begin

            fullAdd_i : fullAdd
            port map(
                bitA      => count_cs(i),
                bitB      => operand_s(i),
                carry_in  => carry_s(i),
                resultBit => result_s(i),
                carry_out => carry_s(i+1)
            );
        
    end generate generateLoopFA;
    -- carry_s(12) sets error_v, see end of process 'combiLogic' above
    
    
    
    nLd_ns    <= nLd;
    valLd_ns  <= valLd;
    updown_ns <= up & down;
     
    sequLogic:
    process (clk) is
    begin
        if(clk = '1' and clk'event) then
            if(nres = '0') then
                err_cs     <= '0';
                count_cs   <= (others=>'0');
                valLd_cs   <= (others=>'0');
                nLd_cs     <= '0';
                updown_cs    <= "00";
            else
                err_cs     <= err_ns;
                count_cs   <= count_ns;
                valLd_cs   <= valLd_ns;
                nLd_cs     <= nLd_ns;
                updown_cs    <= updown_ns;
            end if;
        else
            null;
        end if;
    end process sequLogic;
    
    did <= did_s;
    err <= err_cs;
    cnt <= count_cs;
    
end architecture argl;