--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 11 17:07:34 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm dut -w -dir netgen/fit -ofmt vhdl -sim dut.nga dut_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: dut.nga
-- Output file	: D:\ISE_Projects\dt_s2t1_a3\netgen\fit\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

-- entity dut is
  -- port (
    -- clk : in STD_LOGIC := 'X'; 
    -- nres : in STD_LOGIC := 'X'; 
    -- nLd : in STD_LOGIC := 'X'; 
    -- down : in STD_LOGIC := 'X'; 
    -- up : in STD_LOGIC := 'X'; 
    -- err : out STD_LOGIC; 
    -- valLd : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    -- cnt : out STD_LOGIC_VECTOR ( 11 downto 0 ); 
    -- did : out STD_LOGIC_VECTOR ( 2 downto 0 ) 
  -- );
-- end dut;

architecture Structure of dut is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal nLd_II_UIM_5 : STD_LOGIC; 
  signal down_II_UIM_7 : STD_LOGIC; 
  signal up_II_UIM_9 : STD_LOGIC; 
  signal valLd_0_II_UIM_11 : STD_LOGIC; 
  signal valLd_1_II_UIM_13 : STD_LOGIC; 
  signal valLd_2_II_UIM_15 : STD_LOGIC; 
  signal valLd_3_II_UIM_17 : STD_LOGIC; 
  signal valLd_4_II_UIM_19 : STD_LOGIC; 
  signal valLd_5_II_UIM_21 : STD_LOGIC; 
  signal valLd_6_II_UIM_23 : STD_LOGIC; 
  signal valLd_7_II_UIM_25 : STD_LOGIC; 
  signal valLd_8_II_UIM_27 : STD_LOGIC; 
  signal valLd_9_II_UIM_29 : STD_LOGIC; 
  signal valLd_10_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_UIM_67 : STD_LOGIC; 
  signal cnt_0_MC_D_68 : STD_LOGIC; 
  signal Gnd_69 : STD_LOGIC; 
  signal Vcc_70 : STD_LOGIC; 
  signal cnt_0_MC_D1_71 : STD_LOGIC; 
  signal cnt_0_MC_D2_72 : STD_LOGIC; 
  signal counteri_nLd_cs_73 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_75 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_78 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_2_79 : STD_LOGIC; 
  signal counteri_nLd_cs_MC_Q : STD_LOGIC; 
  signal counteri_nLd_cs_MC_D_81 : STD_LOGIC; 
  signal counteri_nLd_cs_MC_D1_82 : STD_LOGIC; 
  signal counteri_nLd_cs_MC_D2_83 : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_D_85 : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_D1_86 : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_D2_87 : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_D_89 : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_D1_90 : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_D2_91 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D_93 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D1_94 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D2_95 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_UIM_97 : STD_LOGIC; 
  signal cnt_10_MC_D_98 : STD_LOGIC; 
  signal cnt_10_MC_D1_99 : STD_LOGIC; 
  signal cnt_10_MC_D2_100 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_102 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_103 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_104 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_2_105 : STD_LOGIC; 
  signal N_PZ_153_106 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_3_107 : STD_LOGIC; 
  signal cnt_1_MC_UIM_108 : STD_LOGIC; 
  signal cnt_2_MC_UIM_109 : STD_LOGIC; 
  signal cnt_3_MC_UIM_110 : STD_LOGIC; 
  signal cnt_4_MC_UIM_111 : STD_LOGIC; 
  signal cnt_5_MC_UIM_112 : STD_LOGIC; 
  signal cnt_6_MC_UIM_113 : STD_LOGIC; 
  signal cnt_7_MC_UIM_114 : STD_LOGIC; 
  signal cnt_8_MC_UIM_115 : STD_LOGIC; 
  signal cnt_9_MC_UIM_116 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_4_117 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_119 : STD_LOGIC; 
  signal cnt_1_MC_tsimcreated_xor_Q_120 : STD_LOGIC; 
  signal cnt_1_MC_D1_121 : STD_LOGIC; 
  signal cnt_1_MC_D2_122 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_123 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_125 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_126 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_3_127 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_4_128 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D_130 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D1_131 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D2_132 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_134 : STD_LOGIC; 
  signal cnt_2_MC_tsimcreated_xor_Q_135 : STD_LOGIC; 
  signal cnt_2_MC_D1_136 : STD_LOGIC; 
  signal cnt_2_MC_D2_137 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_138 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_140 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_141 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_142 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_143 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D_145 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D1_146 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D2_147 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_149 : STD_LOGIC; 
  signal cnt_3_MC_tsimcreated_xor_Q_150 : STD_LOGIC; 
  signal cnt_3_MC_D1_151 : STD_LOGIC; 
  signal cnt_3_MC_D2_152 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_153 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_155 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_2_156 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_3_157 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_4_158 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D_160 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D1_161 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D2_162 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_164 : STD_LOGIC; 
  signal cnt_4_MC_tsimcreated_xor_Q_165 : STD_LOGIC; 
  signal cnt_4_MC_D1_166 : STD_LOGIC; 
  signal cnt_4_MC_D2_167 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_168 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_170 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_2_171 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_3_172 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_4_173 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D_175 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D1_176 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D2_177 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_179 : STD_LOGIC; 
  signal cnt_5_MC_tsimcreated_xor_Q_180 : STD_LOGIC; 
  signal cnt_5_MC_D1_181 : STD_LOGIC; 
  signal cnt_5_MC_D2_182 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_183 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_185 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_186 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_3_187 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_4_188 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D_190 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D1_191 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D2_192 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_194 : STD_LOGIC; 
  signal cnt_6_MC_tsimcreated_xor_Q_195 : STD_LOGIC; 
  signal cnt_6_MC_D1_196 : STD_LOGIC; 
  signal cnt_6_MC_D2_197 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_198 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_200 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_201 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_202 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_203 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D_205 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D1_206 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D2_207 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_7_MC_D_209 : STD_LOGIC; 
  signal cnt_7_MC_tsimcreated_xor_Q_210 : STD_LOGIC; 
  signal cnt_7_MC_D1_211 : STD_LOGIC; 
  signal cnt_7_MC_D2_212 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_0_213 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_1_215 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_2_216 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_3_217 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_4_218 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D_220 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D1_221 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D2_222 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_8_MC_D_224 : STD_LOGIC; 
  signal cnt_8_MC_tsimcreated_xor_Q_225 : STD_LOGIC; 
  signal cnt_8_MC_D1_226 : STD_LOGIC; 
  signal cnt_8_MC_D2_227 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_0_228 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_1_230 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_2_231 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_3_232 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_4_233 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D_235 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D1_236 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D2_237 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_239 : STD_LOGIC; 
  signal cnt_9_MC_tsimcreated_xor_Q_240 : STD_LOGIC; 
  signal cnt_9_MC_D1_241 : STD_LOGIC; 
  signal cnt_9_MC_D2_242 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_243 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_245 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_246 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_3_247 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_4_248 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D_250 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D1_251 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D2_252 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D_254 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D1_255 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D2_256 : STD_LOGIC; 
  signal N_PZ_153_MC_Q_257 : STD_LOGIC; 
  signal N_PZ_153_MC_D_258 : STD_LOGIC; 
  signal N_PZ_153_MC_D1_259 : STD_LOGIC; 
  signal N_PZ_153_MC_D2_260 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_Q_261 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_D_262 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_D1_263 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_D2_264 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_265 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_266 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_267 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_UIM_269 : STD_LOGIC; 
  signal cnt_11_MC_D_270 : STD_LOGIC; 
  signal cnt_11_MC_tsimcreated_xor_Q_271 : STD_LOGIC; 
  signal cnt_11_MC_D1_272 : STD_LOGIC; 
  signal cnt_11_MC_D2_273 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_274 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_276 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_277 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_278 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D_280 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D1_281 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D2_282 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_283 : STD_LOGIC; 
  signal did_0_MC_D_284 : STD_LOGIC; 
  signal did_0_MC_D1_285 : STD_LOGIC; 
  signal did_0_MC_D2_286 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_287 : STD_LOGIC; 
  signal did_1_MC_D_288 : STD_LOGIC; 
  signal did_1_MC_D1_289 : STD_LOGIC; 
  signal did_1_MC_D2_290 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_291 : STD_LOGIC; 
  signal did_2_MC_D_292 : STD_LOGIC; 
  signal did_2_MC_D1_293 : STD_LOGIC; 
  signal did_2_MC_D2_294 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_UIM_296 : STD_LOGIC; 
  signal err_MC_D_297 : STD_LOGIC; 
  signal err_MC_D1_298 : STD_LOGIC; 
  signal err_MC_D2_299 : STD_LOGIC; 
  signal err_MC_D2_PT_0_300 : STD_LOGIC; 
  signal err_MC_D2_PT_1_301 : STD_LOGIC; 
  signal err_MC_D2_PT_2_302 : STD_LOGIC; 
  signal err_MC_D2_PT_3_303 : STD_LOGIC; 
  signal err_MC_D2_PT_4_304 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D1_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal counteri_valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal counteri_updown_cs : STD_LOGIC_VECTOR ( 1 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_5
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_7
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_9
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_11
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_13
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_15
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_17
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_19
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_21
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_23
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_25
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_27
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_29
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_UIM : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_UIM_67
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_69
    );
  Vcc : X_ONE
    port map (
      O => Vcc_70
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_68
    );
  cnt_0_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D1_IN2,
      O => cnt_0_MC_D1_71
    );
  cnt_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      O => cnt_0_MC_D2_PT_0_75
    );
  cnt_0_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN3,
      O => cnt_0_MC_D2_PT_1_78
    );
  cnt_0_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3,
      O => cnt_0_MC_D2_PT_2_79
    );
  cnt_0_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_IN2,
      O => cnt_0_MC_D2_72
    );
  counteri_nLd_cs : X_BUF
    port map (
      I => counteri_nLd_cs_MC_Q,
      O => counteri_nLd_cs_73
    );
  counteri_nLd_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_nLd_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_nLd_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_nLd_cs_MC_Q
    );
  counteri_nLd_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_nLd_cs_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_nLd_cs_MC_D_IN1,
      O => counteri_nLd_cs_MC_D_81
    );
  counteri_nLd_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN1,
      O => counteri_nLd_cs_MC_D1_82
    );
  counteri_nLd_cs_MC_D2 : X_ZERO
    port map (
      O => counteri_nLd_cs_MC_D2_83
    );
  counteri_updown_cs_0_Q : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_Q,
      O => counteri_updown_cs(0)
    );
  counteri_updown_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_updown_cs_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_updown_cs_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_updown_cs_0_MC_Q
    );
  counteri_updown_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN1,
      O => counteri_updown_cs_0_MC_D_85
    );
  counteri_updown_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN1,
      O => counteri_updown_cs_0_MC_D1_86
    );
  counteri_updown_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_updown_cs_0_MC_D2_87
    );
  counteri_updown_cs_1_Q : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_Q,
      O => counteri_updown_cs(1)
    );
  counteri_updown_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_updown_cs_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_updown_cs_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_updown_cs_1_MC_Q
    );
  counteri_updown_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN1,
      O => counteri_updown_cs_1_MC_D_89
    );
  counteri_updown_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN1,
      O => counteri_updown_cs_1_MC_D1_90
    );
  counteri_updown_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_updown_cs_1_MC_D2_91
    );
  counteri_valLd_cs_0_Q : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_Q,
      O => counteri_valLd_cs(0)
    );
  counteri_valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_0_MC_Q
    );
  counteri_valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1,
      O => counteri_valLd_cs_0_MC_D_93
    );
  counteri_valLd_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1,
      O => counteri_valLd_cs_0_MC_D1_94
    );
  counteri_valLd_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_0_MC_D2_95
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_UIM : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_UIM_97
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_98
    );
  cnt_10_MC_D1 : X_ZERO
    port map (
      O => cnt_10_MC_D1_99
    );
  cnt_10_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2,
      O => cnt_10_MC_D2_PT_0_102
    );
  cnt_10_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN3,
      O => cnt_10_MC_D2_PT_1_104
    );
  cnt_10_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN4,
      O => cnt_10_MC_D2_PT_2_105
    );
  cnt_10_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4,
      O => cnt_10_MC_D2_PT_3_107
    );
  cnt_10_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN15,
      O => cnt_10_MC_D2_PT_4_117
    );
  cnt_10_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_IN4,
      O => cnt_10_MC_D2_100
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_UIM : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_UIM_108
    );
  cnt_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_MC_tsimcreated_xor_Q_120
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_119
    );
  cnt_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_MC_D1_121
    );
  cnt_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      O => cnt_1_MC_D2_PT_0_123
    );
  cnt_1_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2,
      O => cnt_1_MC_D2_PT_1_125
    );
  cnt_1_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3,
      O => cnt_1_MC_D2_PT_2_126
    );
  cnt_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4,
      O => cnt_1_MC_D2_PT_3_127
    );
  cnt_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4,
      O => cnt_1_MC_D2_PT_4_128
    );
  cnt_1_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_IN4,
      O => cnt_1_MC_D2_122
    );
  counteri_valLd_cs_1_Q : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_Q,
      O => counteri_valLd_cs(1)
    );
  counteri_valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_1_MC_Q
    );
  counteri_valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1,
      O => counteri_valLd_cs_1_MC_D_130
    );
  counteri_valLd_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1,
      O => counteri_valLd_cs_1_MC_D1_131
    );
  counteri_valLd_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_1_MC_D2_132
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_UIM : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_UIM_109
    );
  cnt_2_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1,
      O => cnt_2_MC_tsimcreated_xor_Q_135
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_134
    );
  cnt_2_MC_D1 : X_ZERO
    port map (
      O => cnt_2_MC_D1_136
    );
  cnt_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      O => cnt_2_MC_D2_PT_0_138
    );
  cnt_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      O => cnt_2_MC_D2_PT_1_140
    );
  cnt_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => cnt_2_MC_D2_PT_2_141
    );
  cnt_2_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN5,
      O => cnt_2_MC_D2_PT_3_142
    );
  cnt_2_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN5,
      O => cnt_2_MC_D2_PT_4_143
    );
  cnt_2_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      O => cnt_2_MC_D2_137
    );
  counteri_valLd_cs_2_Q : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_Q,
      O => counteri_valLd_cs(2)
    );
  counteri_valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_2_MC_Q
    );
  counteri_valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1,
      O => counteri_valLd_cs_2_MC_D_145
    );
  counteri_valLd_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1,
      O => counteri_valLd_cs_2_MC_D1_146
    );
  counteri_valLd_cs_2_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_2_MC_D2_147
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_UIM : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_UIM_110
    );
  cnt_3_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1,
      O => cnt_3_MC_tsimcreated_xor_Q_150
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_149
    );
  cnt_3_MC_D1 : X_ZERO
    port map (
      O => cnt_3_MC_D1_151
    );
  cnt_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      O => cnt_3_MC_D2_PT_0_153
    );
  cnt_3_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2,
      O => cnt_3_MC_D2_PT_1_155
    );
  cnt_3_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3,
      O => cnt_3_MC_D2_PT_2_156
    );
  cnt_3_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6,
      O => cnt_3_MC_D2_PT_3_157
    );
  cnt_3_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN6,
      O => cnt_3_MC_D2_PT_4_158
    );
  cnt_3_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_IN4,
      O => cnt_3_MC_D2_152
    );
  counteri_valLd_cs_3_Q : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_Q,
      O => counteri_valLd_cs(3)
    );
  counteri_valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_3_MC_Q
    );
  counteri_valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1,
      O => counteri_valLd_cs_3_MC_D_160
    );
  counteri_valLd_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1,
      O => counteri_valLd_cs_3_MC_D1_161
    );
  counteri_valLd_cs_3_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_3_MC_D2_162
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_UIM : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_UIM_111
    );
  cnt_4_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1,
      O => cnt_4_MC_tsimcreated_xor_Q_165
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_164
    );
  cnt_4_MC_D1 : X_ZERO
    port map (
      O => cnt_4_MC_D1_166
    );
  cnt_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      O => cnt_4_MC_D2_PT_0_168
    );
  cnt_4_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2,
      O => cnt_4_MC_D2_PT_1_170
    );
  cnt_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3,
      O => cnt_4_MC_D2_PT_2_171
    );
  cnt_4_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7,
      O => cnt_4_MC_D2_PT_3_172
    );
  cnt_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7,
      O => cnt_4_MC_D2_PT_4_173
    );
  cnt_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_IN4,
      O => cnt_4_MC_D2_167
    );
  counteri_valLd_cs_4_Q : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_Q,
      O => counteri_valLd_cs(4)
    );
  counteri_valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_4_MC_Q
    );
  counteri_valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1,
      O => counteri_valLd_cs_4_MC_D_175
    );
  counteri_valLd_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1,
      O => counteri_valLd_cs_4_MC_D1_176
    );
  counteri_valLd_cs_4_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_4_MC_D2_177
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_UIM : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_UIM_112
    );
  cnt_5_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1,
      O => cnt_5_MC_tsimcreated_xor_Q_180
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_179
    );
  cnt_5_MC_D1 : X_ZERO
    port map (
      O => cnt_5_MC_D1_181
    );
  cnt_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      O => cnt_5_MC_D2_PT_0_183
    );
  cnt_5_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2,
      O => cnt_5_MC_D2_PT_1_185
    );
  cnt_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      O => cnt_5_MC_D2_PT_2_186
    );
  cnt_5_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN15,
      O => cnt_5_MC_D2_PT_3_187
    );
  cnt_5_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15,
      O => cnt_5_MC_D2_PT_4_188
    );
  cnt_5_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_IN4,
      O => cnt_5_MC_D2_182
    );
  counteri_valLd_cs_5_Q : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_Q,
      O => counteri_valLd_cs(5)
    );
  counteri_valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_5_MC_Q
    );
  counteri_valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1,
      O => counteri_valLd_cs_5_MC_D_190
    );
  counteri_valLd_cs_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1,
      O => counteri_valLd_cs_5_MC_D1_191
    );
  counteri_valLd_cs_5_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_5_MC_D2_192
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_UIM : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_UIM_113
    );
  cnt_6_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1,
      O => cnt_6_MC_tsimcreated_xor_Q_195
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_194
    );
  cnt_6_MC_D1 : X_ZERO
    port map (
      O => cnt_6_MC_D1_196
    );
  cnt_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      O => cnt_6_MC_D2_PT_0_198
    );
  cnt_6_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2,
      O => cnt_6_MC_D2_PT_1_200
    );
  cnt_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      O => cnt_6_MC_D2_PT_2_201
    );
  cnt_6_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN15,
      O => cnt_6_MC_D2_PT_3_202
    );
  cnt_6_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15,
      O => cnt_6_MC_D2_PT_4_203
    );
  cnt_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      O => cnt_6_MC_D2_197
    );
  counteri_valLd_cs_6_Q : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_Q,
      O => counteri_valLd_cs(6)
    );
  counteri_valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_6_MC_Q
    );
  counteri_valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1,
      O => counteri_valLd_cs_6_MC_D_205
    );
  counteri_valLd_cs_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1,
      O => counteri_valLd_cs_6_MC_D1_206
    );
  counteri_valLd_cs_6_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_6_MC_D2_207
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_UIM : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_UIM_114
    );
  cnt_7_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1,
      O => cnt_7_MC_tsimcreated_xor_Q_210
    );
  cnt_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_7_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_7_MC_Q_tsimrenamed_net_Q
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_209
    );
  cnt_7_MC_D1 : X_ZERO
    port map (
      O => cnt_7_MC_D1_211
    );
  cnt_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1,
      O => cnt_7_MC_D2_PT_0_213
    );
  cnt_7_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2,
      O => cnt_7_MC_D2_PT_1_215
    );
  cnt_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3,
      O => cnt_7_MC_D2_PT_2_216
    );
  cnt_7_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN15,
      O => cnt_7_MC_D2_PT_3_217
    );
  cnt_7_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN9,
      I10 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN15,
      O => cnt_7_MC_D2_PT_4_218
    );
  cnt_7_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_IN4,
      O => cnt_7_MC_D2_212
    );
  counteri_valLd_cs_7_Q : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_Q,
      O => counteri_valLd_cs(7)
    );
  counteri_valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_7_MC_Q
    );
  counteri_valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1,
      O => counteri_valLd_cs_7_MC_D_220
    );
  counteri_valLd_cs_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1,
      O => counteri_valLd_cs_7_MC_D1_221
    );
  counteri_valLd_cs_7_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_7_MC_D2_222
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_UIM : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_UIM_115
    );
  cnt_8_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1,
      O => cnt_8_MC_tsimcreated_xor_Q_225
    );
  cnt_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_8_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_8_MC_Q_tsimrenamed_net_Q
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_224
    );
  cnt_8_MC_D1 : X_ZERO
    port map (
      O => cnt_8_MC_D1_226
    );
  cnt_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1,
      O => cnt_8_MC_D2_PT_0_228
    );
  cnt_8_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN2,
      O => cnt_8_MC_D2_PT_1_230
    );
  cnt_8_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3,
      O => cnt_8_MC_D2_PT_2_231
    );
  cnt_8_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN15,
      O => cnt_8_MC_D2_PT_3_232
    );
  cnt_8_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN9,
      I10 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN10,
      I11 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN15,
      O => cnt_8_MC_D2_PT_4_233
    );
  cnt_8_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_IN4,
      O => cnt_8_MC_D2_227
    );
  counteri_valLd_cs_8_Q : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_Q,
      O => counteri_valLd_cs(8)
    );
  counteri_valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_8_MC_Q
    );
  counteri_valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1,
      O => counteri_valLd_cs_8_MC_D_235
    );
  counteri_valLd_cs_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1,
      O => counteri_valLd_cs_8_MC_D1_236
    );
  counteri_valLd_cs_8_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_8_MC_D2_237
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_UIM : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_UIM_116
    );
  cnt_9_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1,
      O => cnt_9_MC_tsimcreated_xor_Q_240
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_239
    );
  cnt_9_MC_D1 : X_ZERO
    port map (
      O => cnt_9_MC_D1_241
    );
  cnt_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      O => cnt_9_MC_D2_PT_0_243
    );
  cnt_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2,
      O => cnt_9_MC_D2_PT_1_245
    );
  cnt_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      O => cnt_9_MC_D2_PT_2_246
    );
  cnt_9_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15,
      O => cnt_9_MC_D2_PT_3_247
    );
  cnt_9_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN9,
      I10 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN10,
      I11 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN11,
      I12 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN15,
      O => cnt_9_MC_D2_PT_4_248
    );
  cnt_9_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_IN4,
      O => cnt_9_MC_D2_242
    );
  counteri_valLd_cs_9_Q : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_Q,
      O => counteri_valLd_cs(9)
    );
  counteri_valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_9_MC_Q
    );
  counteri_valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1,
      O => counteri_valLd_cs_9_MC_D_250
    );
  counteri_valLd_cs_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1,
      O => counteri_valLd_cs_9_MC_D1_251
    );
  counteri_valLd_cs_9_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_9_MC_D2_252
    );
  counteri_valLd_cs_10_Q : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_Q,
      O => counteri_valLd_cs(10)
    );
  counteri_valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_10_MC_Q
    );
  counteri_valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1,
      O => counteri_valLd_cs_10_MC_D_254
    );
  counteri_valLd_cs_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1,
      O => counteri_valLd_cs_10_MC_D1_255
    );
  counteri_valLd_cs_10_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_10_MC_D2_256
    );
  N_PZ_153 : X_BUF
    port map (
      I => N_PZ_153_MC_Q_257,
      O => N_PZ_153_106
    );
  N_PZ_153_MC_Q : X_BUF
    port map (
      I => N_PZ_153_MC_D_258,
      O => N_PZ_153_MC_Q_257
    );
  N_PZ_153_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_153_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_153_MC_D_IN1,
      O => N_PZ_153_MC_D_258
    );
  N_PZ_153_MC_D1 : X_AND16
    port map (
      I0 => NlwInverterSignal_N_PZ_153_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_153_MC_D1_IN1,
      I2 => NlwInverterSignal_N_PZ_153_MC_D1_IN2,
      I3 => NlwInverterSignal_N_PZ_153_MC_D1_IN3,
      I4 => NlwInverterSignal_N_PZ_153_MC_D1_IN4,
      I5 => NlwInverterSignal_N_PZ_153_MC_D1_IN5,
      I6 => NlwInverterSignal_N_PZ_153_MC_D1_IN6,
      I7 => NlwInverterSignal_N_PZ_153_MC_D1_IN7,
      I8 => NlwInverterSignal_N_PZ_153_MC_D1_IN8,
      I9 => NlwInverterSignal_N_PZ_153_MC_D1_IN9,
      I10 => NlwBufferSignal_N_PZ_153_MC_D1_IN10,
      I11 => NlwBufferSignal_N_PZ_153_MC_D1_IN11,
      I12 => NlwBufferSignal_N_PZ_153_MC_D1_IN12,
      I13 => NlwBufferSignal_N_PZ_153_MC_D1_IN13,
      I14 => NlwBufferSignal_N_PZ_153_MC_D1_IN14,
      I15 => NlwBufferSignal_N_PZ_153_MC_D1_IN15,
      O => N_PZ_153_MC_D1_259
    );
  N_PZ_153_MC_D2 : X_ZERO
    port map (
      O => N_PZ_153_MC_D2_260
    );
  counteri_Madd_result_v_add0000_or0009 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_Q_261,
      O => counteri_Madd_result_v_add0000_or0009_103
    );
  counteri_Madd_result_v_add0000_or0009_MC_Q : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_D_262,
      O => counteri_Madd_result_v_add0000_or0009_MC_Q_261
    );
  counteri_Madd_result_v_add0000_or0009_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D_IN1,
      O => counteri_Madd_result_v_add0000_or0009_MC_D_262
    );
  counteri_Madd_result_v_add0000_or0009_MC_D1 : X_ZERO
    port map (
      O => counteri_Madd_result_v_add0000_or0009_MC_D1_263
    );
  counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN2,
      O => counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_265
    );
  counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2,
      O => counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_266
    );
  counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN7,
      I8 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN15,
      O => counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_267
    );
  counteri_Madd_result_v_add0000_or0009_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN0,
      I1 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN1,
      I2 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN2,
      O => counteri_Madd_result_v_add0000_or0009_MC_D2_264
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_UIM : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_UIM_269
    );
  cnt_11_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1,
      O => cnt_11_MC_tsimcreated_xor_Q_271
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_270
    );
  cnt_11_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D1_IN2,
      O => cnt_11_MC_D1_272
    );
  cnt_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => cnt_11_MC_D2_PT_0_274
    );
  cnt_11_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2,
      O => cnt_11_MC_D2_PT_1_276
    );
  cnt_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3,
      O => cnt_11_MC_D2_PT_2_277
    );
  cnt_11_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      O => cnt_11_MC_D2_PT_3_278
    );
  cnt_11_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      O => cnt_11_MC_D2_273
    );
  counteri_valLd_cs_11_Q : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_Q,
      O => counteri_valLd_cs(11)
    );
  counteri_valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_11_MC_Q
    );
  counteri_valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1,
      O => counteri_valLd_cs_11_MC_D_280
    );
  counteri_valLd_cs_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1,
      O => counteri_valLd_cs_11_MC_D1_281
    );
  counteri_valLd_cs_11_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_11_MC_D2_282
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_283,
      O => did_0_MC_Q_59
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_284,
      O => did_0_MC_Q_tsimrenamed_net_Q_283
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_284
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_285
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_286
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_287,
      O => did_1_MC_Q_61
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_288,
      O => did_1_MC_Q_tsimrenamed_net_Q_287
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_288
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_289
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_290
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_291,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_292,
      O => did_2_MC_Q_tsimrenamed_net_Q_291
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_292
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_293
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_294
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_UIM : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_UIM_296
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_297
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_298
    );
  err_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_0_IN1,
      O => err_MC_D2_PT_0_300
    );
  err_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_1_IN1,
      O => err_MC_D2_PT_1_301
    );
  err_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_2_IN2,
      O => err_MC_D2_PT_2_302
    );
  err_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_3_IN3,
      O => err_MC_D2_PT_3_303
    );
  err_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_4_IN3,
      O => err_MC_D2_PT_4_304
    );
  err_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      I4 => NlwBufferSignal_err_MC_D2_IN4,
      O => err_MC_D2_299
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_68,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_72,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_75,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_78,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_2_79,
      O => NlwBufferSignal_cnt_0_MC_D2_IN2
    );
  NlwBufferBlock_counteri_nLd_cs_MC_REG_IN : X_BUF
    port map (
      I => counteri_nLd_cs_MC_D_81,
      O => NlwBufferSignal_counteri_nLd_cs_MC_REG_IN
    );
  NlwBufferBlock_counteri_nLd_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_nLd_cs_MC_REG_CLK
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_MC_D1_82,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D_IN0
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_MC_D2_83,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D_IN1
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN0
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_5,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN1
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_D_85,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_D1_86,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_D2_87,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_7,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_D_89,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_D1_90,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_D2_91,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_9,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D_93,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D1_94,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D2_95,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_0_II_UIM_11,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_D_98,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_99,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_100,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_103,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_103,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_153_106,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => cnt_9_MC_UIM_116,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_102,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_104,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_2_105,
      O => NlwBufferSignal_cnt_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_3_107,
      O => NlwBufferSignal_cnt_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_4_117,
      O => NlwBufferSignal_cnt_10_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D_119,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_tsimcreated_xor_Q_120,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_121,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_122,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_123,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_125,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_126,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_3_127,
      O => NlwBufferSignal_cnt_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_4_128,
      O => NlwBufferSignal_cnt_1_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D_130,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D1_131,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D2_132,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_1_II_UIM_13,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_2_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D_134,
      O => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_2_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_tsimcreated_xor_Q_135,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_136,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_137,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_138,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_140,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_141,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_142,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_143,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D_145,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D1_146,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D2_147,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_2_II_UIM_15,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_cnt_3_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D_149,
      O => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_3_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_tsimcreated_xor_Q_150,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_151,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_152,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_153,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_155,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_2_156,
      O => NlwBufferSignal_cnt_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_3_157,
      O => NlwBufferSignal_cnt_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_4_158,
      O => NlwBufferSignal_cnt_3_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D_160,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D1_161,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D2_162,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_3_II_UIM_17,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D_164,
      O => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_4_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_tsimcreated_xor_Q_165,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_166,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_167,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_168,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_170,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_2_171,
      O => NlwBufferSignal_cnt_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_3_172,
      O => NlwBufferSignal_cnt_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_4_173,
      O => NlwBufferSignal_cnt_4_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D_175,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D1_176,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D2_177,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_4_II_UIM_19,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D_179,
      O => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_5_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_tsimcreated_xor_Q_180,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_181,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_182,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_183,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_185,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_186,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_3_187,
      O => NlwBufferSignal_cnt_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_4_188,
      O => NlwBufferSignal_cnt_5_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D_190,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D1_191,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D2_192,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_5_II_UIM_21,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D_194,
      O => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_6_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_tsimcreated_xor_Q_195,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_196,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_197,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_198,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_200,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_201,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_202,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_203,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D_205,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D1_206,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D2_207,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_6_II_UIM_23,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D_209,
      O => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_7_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_MC_tsimcreated_xor_Q_210,
      O => NlwBufferSignal_cnt_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_211,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_212,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_0_213,
      O => NlwBufferSignal_cnt_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_1_215,
      O => NlwBufferSignal_cnt_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_2_216,
      O => NlwBufferSignal_cnt_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_3_217,
      O => NlwBufferSignal_cnt_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_4_218,
      O => NlwBufferSignal_cnt_7_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D_220,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D1_221,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D2_222,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_7_II_UIM_25,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D_224,
      O => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_8_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_MC_tsimcreated_xor_Q_225,
      O => NlwBufferSignal_cnt_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_226,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_227,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_0_228,
      O => NlwBufferSignal_cnt_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_1_230,
      O => NlwBufferSignal_cnt_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_2_231,
      O => NlwBufferSignal_cnt_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_3_232,
      O => NlwBufferSignal_cnt_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_4_233,
      O => NlwBufferSignal_cnt_8_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D_235,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D1_236,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D2_237,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_27,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D_239,
      O => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_9_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_tsimcreated_xor_Q_240,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_241,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_242,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_116,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_116,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_UIM_116,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_243,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_245,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_246,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_3_247,
      O => NlwBufferSignal_cnt_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_4_248,
      O => NlwBufferSignal_cnt_9_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D_250,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D1_251,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D2_252,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_9_II_UIM_29,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D_254,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D1_255,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D2_256,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_10_II_UIM_31,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_153_MC_D1_259,
      O => NlwBufferSignal_N_PZ_153_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_153_MC_D2_260,
      O => NlwBufferSignal_N_PZ_153_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN2
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN3
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN4 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN4
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN5 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN5
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN6 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN6
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN7 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN7
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN8 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN8
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN9 : X_BUF
    port map (
      I => cnt_9_MC_UIM_116,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN9
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN10
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN11
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN12
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN13
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN14
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN15
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D_IN0 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_D1_263,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D_IN1 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_D2_264,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_153_106,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_108,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_109,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_3_MC_UIM_110,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_4_MC_UIM_111,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_112,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_6_MC_UIM_113,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_7_MC_UIM_114,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => cnt_8_MC_UIM_115,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => cnt_9_MC_UIM_116,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_IN0 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_265,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_IN1 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_266,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_267,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D_270,
      O => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_11_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_tsimcreated_xor_Q_271,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_272,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_273,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D1_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_103,
      O => NlwBufferSignal_cnt_11_MC_D1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_269,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_269,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_11_MC_UIM_269,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_274,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_276,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_277,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_278,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D_280,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D1_281,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D2_282,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_285,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_286,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_289,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_290,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_293,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_294,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_297,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_298,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_299,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_103,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_269,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => err_MC_UIM_296,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0009_103,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => err_MC_UIM_296,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_11_MC_UIM_269,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => err_MC_UIM_296,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_300,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_301,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_302,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_303,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwBufferBlock_err_MC_D2_IN4 : X_BUF
    port map (
      I => err_MC_D2_PT_4_304,
      O => NlwBufferSignal_err_MC_D2_IN4
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN10,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN10
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN10,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN10
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN11,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN11
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN10,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN10
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN11,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN11
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN12,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN12
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN0
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN2,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN2
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN3,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN3
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN4,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN4
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN5,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN5
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN6,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN6
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN7 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN7,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN7
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN8 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN8,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN8
    );
  NlwInverterBlock_N_PZ_153_MC_D1_IN9 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D1_IN9,
      O => NlwInverterSignal_N_PZ_153_MC_D1_IN9
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0009_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_did_2_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_did_2_MC_D_IN0,
      O => NlwInverterSignal_did_2_MC_D_IN0
    );
  NlwInverterBlock_err_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D_IN0,
      O => NlwInverterSignal_err_MC_D_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN3
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

