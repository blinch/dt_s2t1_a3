-- up/down counter for 11bit value range
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity counter is
    port (
        did     : out std_logic_vector(  2 downto 0);
        err     : out std_logic;
        cnt     : out std_logic_vector( 11 downto 0);
        --
        valLd   : in  std_logic_vector( 11 downto 0);
        nLd     : in  std_logic;
        up      : in  std_logic;
        down    : in  std_logic;
        --
        clk     : in  std_logic;
        nres    : in  std_logic
    );
end entity counter;