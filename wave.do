onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb/did
add wave -noupdate -divider -height 22 Error
add wave -noupdate /tb/err
add wave -noupdate -divider -height 22 Counter
add wave -noupdate -expand /tb/count
add wave -noupdate -divider -height 22 {Value Load}
add wave -noupdate /tb/valLd
add wave -noupdate -divider -height 22 {not Load}
add wave -noupdate /tb/nLd
add wave -noupdate -divider -height 22 UP
add wave -noupdate /tb/up
add wave -noupdate -divider -height 22 DOWN
add wave -noupdate /tb/down
add wave -noupdate -divider -height 22 clock
add wave -noupdate /tb/clk
add wave -noupdate -divider -height 22 {not reset}
add wave -noupdate /tb/nres
add wave -noupdate -divider -height 22 Okay
add wave -noupdate /tb/rci/ok_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {144247 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {945 ns}
