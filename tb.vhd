-- test bench for 11bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity tb is 
end entity tb;



architecture arc of tb is


    signal did     : std_logic_vector(  2 downto 0);
    signal err     : std_logic;
    signal count   : std_logic_vector( 11 downto 0);
    --
    signal valLd   : std_logic_vector( 11 downto 0);
    signal nLd     : std_logic;
    signal up      : std_logic;
    signal down    : std_logic;
    --
    signal clk     : std_logic;
    signal nres    : std_logic;


    component DUT is
        port(
            did     : out std_logic_vector(  2 downto 0);
            err     : out std_logic;
            cnt     : out std_logic_vector( 11 downto 0);
            --
            valLd   : in  std_logic_vector( 11 downto 0);
            nLd     : in  std_logic;
            up      : in  std_logic;
            down    : in  std_logic;
            --
            clk     : in  std_logic;
            nres    : in  std_logic
        );
    end component DUT;

    for all : DUT use entity work.dut(arc);


    component stimuliGen is
        port(
            up    : out  std_logic;
            down  : out  std_logic;
            valLd : out  std_logic_vector(11 downto 0);
            nLd   : out  std_logic;
            --
            clk  : out std_logic;
            nres : out std_logic
        );
    end component stimuliGen;

    for all : stimuliGen use entity work.sg(arc);

	component responseCheck is
		port(
			count : in std_logic_vector(11 downto 0);
			did   : in std_logic_vector(2 downto 0);
			err   : in std_logic
			);
	end component responseCheck;
	
	for all : responseCheck use entity work.responseCheck(arc);


begin
    duti : DUT
        port map(
            did     => did,
            err     => err,
            cnt     => count,
            --
            up      => up,
            down    => down,
            valLd   => valLd,
            nLd     => nLd,
            --
            clk     => clk,
            nres    => nres
        );
        
    sgi : stimuliGen
        port map(
            up    => up,
            down  => down,
            valLd => valLd,
            nLd   => nLd,
            --
            clk   => clk,
            nres  => nres
        );
        
    rci : responseCheck
       port map(
	    count => count,
        did => did,
        err =>  err
    );

end architecture arc;