-- architecture aril - operator overloading - for 11bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_unsigned.all;
    
architecture aril of counter is
    -- 001 aril
    -- 010 argl
    -- 100 arop
    signal did_s    : std_logic_vector(  2 downto 0) := "001";
    signal err_ns   : std_logic;
    signal err_cs   : std_logic := '0';
    signal count_ns : std_logic_vector( 11 downto 0);
    signal count_cs : std_logic_vector( 11 downto 0) := (others=>'0');
    --
    signal valLd_ns   : std_logic_vector( 11 downto 0);
    signal valLd_cs   : std_logic_vector( 11 downto 0) := (others=>'0');
    signal nLd_ns     : std_logic;
    signal nLd_cs     : std_logic := '0';
    signal updown_ns  : std_logic_vector ( 1 downto 0);
    signal updown_cs  : std_logic_vector ( 1 downto 0) := "00";
    
begin

    
    combiLogic:
    process (updown_cs, count_cs, nLd_cs, valLd_cs, err_cs) is 
        variable count_v   : std_logic_vector(12 downto 0);
        variable result_v  : std_logic_vector(12 downto 0);
        variable operand_v : std_logic_vector(12 downto 0);
        variable updown_v  : std_logic_vector(1 downto 0);
        variable err_v     : std_logic;
        variable carry_v   : std_logic;
    begin
        err_v := err_cs;
        operand_v := (others => '0');
        count_v := '0' & count_cs;
        carry_v := '0';
        updown_v := updown_cs;
        
        if(nLd_cs = '0') then         -- load new count value
            result_v := valLd_cs(valLd_cs'high) & valLd_cs;
			err_v := '0';
        else                        -- count:
            result_v := (others => '0'); -- because of latch warning in ISE
            case updown_v is
                when "01" =>        -- down
                    operand_v := (others => '1');
                when "10" =>        -- up
                    operand_v    := (others => '0');
                    operand_v(0) := '1';
                when others =>        -- do nothing
                    null;
            end case;
            ------------------------------------
            fullAdderLoop:
            for i in 0 to 12 loop
               result_v(i) := count_v(i) XOR operand_v(i) XOR carry_v;
               carry_v := (count_v(i) AND operand_v(i)) OR (count_v(i) AND carry_v) OR (operand_v(i) AND carry_v);
            end loop fullAdderLoop;

            count_ns <= result_v(11 downto 0);
            err_ns <= (carry_v XOR updown_v(0)) OR err_v; -- updown(0) --> down bit
            ------------------------------------        

			if(result_v(result_v'high-1) /= result_v(result_v'high)) then -- fail!
				err_v := '1';
			else
				err_v := '0' or err_v;
			end if;
		end if;
        
        count_ns <= result_v(11 downto 0);
        err_ns   <= err_v;
    end process combiLogic;
    
    
    
    
    
    nLd_ns    <= nLd;
    valLd_ns  <= valLd;
    updown_ns <= up & down;
     
    sequLogic:
    process (clk) is
    begin
        if(clk = '1' and clk'event) then
            if(nres = '0') then
                err_cs     <= '0';
                count_cs   <= (others=>'0');
                valLd_cs   <= (others=>'0');
                nLd_cs     <= '0';
                updown_cs    <= "00";
            else
                err_cs     <= err_ns;
                count_cs   <= count_ns;
                valLd_cs   <= valLd_ns;
                nLd_cs     <= nLd_ns;
                updown_cs    <= updown_ns;
            end if;
        else
            null;
        end if;
    end process sequLogic;
    
    did <= did_s;
    err <= err_cs;
    cnt <= count_cs;
end architecture aril;