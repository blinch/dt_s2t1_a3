-- entity to instantiate all 3 different architectures for 11bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;



entity dut is
    port (
        did     : out std_logic_vector(  2 downto 0);
        err     : out std_logic;
        cnt     : out std_logic_vector( 11 downto 0);
        
        valLd   : in  std_logic_vector( 11 downto 0);
        nLd     : in  std_logic;
        up      : in  std_logic;
        down    : in  std_logic;
        
        clk     : in  std_logic;
        nres    : in  std_logic
    );
end entity dut;



architecture arc of dut is
    component counter is
        port(
            did     : out std_logic_vector(  2 downto 0);
            err     : out std_logic;
            cnt     : out std_logic_vector( 11 downto 0);
            --
            valLd   : in  std_logic_vector( 11 downto 0);
            nLd     : in  std_logic;
            up      : in  std_logic;
            down    : in  std_logic;
            --
            clk     : in  std_logic;
            nres    : in  std_logic
        );
    end component counter;
    for all : counter use entity work.counter(argl);
    -- insert architecture you want to test in line above here
    -- argl arop aril

begin
    counteri : counter
        port map(
            did   => did,
            err   => err,
            cnt   => cnt,
            --
            valLd => valLd,
            nLd   => nLd,
            up    => up,
            down  => down,
            --
            clk   => clk,
            nres  => nres
        );
end architecture arc;