-- full adder for 11bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3

-- adds just two bits and carry_in
-- result is just one bit and carry_out
-- this entity must be concatinated to be useful for the counter

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
    
entity fullAdd is
    port(     
        --operation : in std_logic; -- 0 -> addition   |   1 -> subtraction
        bitA      : in std_logic;
        bitB      : in std_logic;
        carry_in  : in std_logic;
        resultBit : out std_logic;
        carry_out : out std_logic
    );
end entity fullAdd;

architecture arc of fullAdd is
begin
    combiLogic:
    process (bitA, bitB, carry_in) is
        variable bitA_v      : std_logic;
        variable bitB_v      : std_logic;
        variable carry_in_v  : std_logic;
    
        variable result_v    : std_logic;
        variable carry_out_v : std_logic;
    
    begin
        -- step1
        bitA_v     := bitA;
        bitB_v     := bitB;
        carry_in_v := carry_in;
        
        -- step2
        result_v    := bitA_v xor bitB_v xor carry_in_v;
        carry_out_v := (bitA_v and bitB_v) or ((bitA_v xor bitB_v) and carry_in_v);
        
        resultBit <= result_v;
        carry_out <= carry_out_v;
    end process combiLogic;
end architecture arc;