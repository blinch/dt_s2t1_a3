--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 11 17:04:41 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm dut -w -dir netgen/fit -ofmt vhdl -sim dut.nga dut_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: dut.nga
-- Output file	: D:\ISE_Projects\dt_s2t1_a3\netgen\fit\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity dut is
  port (
    clk : in STD_LOGIC := 'X'; 
    nres : in STD_LOGIC := 'X'; 
    nLd : in STD_LOGIC := 'X'; 
    down : in STD_LOGIC := 'X'; 
    up : in STD_LOGIC := 'X'; 
    err : out STD_LOGIC; 
    valLd : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    cnt : out STD_LOGIC_VECTOR ( 11 downto 0 ); 
    did : out STD_LOGIC_VECTOR ( 2 downto 0 ) 
  );
end dut;

architecture Structure of dut is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal nLd_II_UIM_5 : STD_LOGIC; 
  signal down_II_UIM_7 : STD_LOGIC; 
  signal up_II_UIM_9 : STD_LOGIC; 
  signal valLd_0_II_UIM_11 : STD_LOGIC; 
  signal valLd_1_II_UIM_13 : STD_LOGIC; 
  signal valLd_2_II_UIM_15 : STD_LOGIC; 
  signal valLd_3_II_UIM_17 : STD_LOGIC; 
  signal valLd_4_II_UIM_19 : STD_LOGIC; 
  signal valLd_5_II_UIM_21 : STD_LOGIC; 
  signal valLd_6_II_UIM_23 : STD_LOGIC; 
  signal valLd_7_II_UIM_25 : STD_LOGIC; 
  signal valLd_8_II_UIM_27 : STD_LOGIC; 
  signal valLd_9_II_UIM_29 : STD_LOGIC; 
  signal valLd_10_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_UIM_67 : STD_LOGIC; 
  signal cnt_0_MC_D_68 : STD_LOGIC; 
  signal Gnd_69 : STD_LOGIC; 
  signal Vcc_70 : STD_LOGIC; 
  signal cnt_0_MC_D1_71 : STD_LOGIC; 
  signal cnt_0_MC_D2_72 : STD_LOGIC; 
  signal counteri_nLd_cs_73 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_75 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_78 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_2_79 : STD_LOGIC; 
  signal counteri_nLd_cs_MC_Q : STD_LOGIC; 
  signal counteri_nLd_cs_MC_D_81 : STD_LOGIC; 
  signal counteri_nLd_cs_MC_D1_82 : STD_LOGIC; 
  signal counteri_nLd_cs_MC_D2_83 : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_D_85 : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_D1_86 : STD_LOGIC; 
  signal counteri_updown_cs_0_MC_D2_87 : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_D_89 : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_D1_90 : STD_LOGIC; 
  signal counteri_updown_cs_1_MC_D2_91 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D_93 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D1_94 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D2_95 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_UIM_97 : STD_LOGIC; 
  signal cnt_10_MC_D_98 : STD_LOGIC; 
  signal cnt_10_MC_tsimcreated_xor_Q_99 : STD_LOGIC; 
  signal cnt_10_MC_D1_100 : STD_LOGIC; 
  signal cnt_10_MC_D2_101 : STD_LOGIC; 
  signal counteri_carry_v_or0006_102 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_103 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_105 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_2_106 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_3_107 : STD_LOGIC; 
  signal counteri_carry_v_or0006_MC_Q_108 : STD_LOGIC; 
  signal counteri_carry_v_or0006_MC_D_109 : STD_LOGIC; 
  signal counteri_carry_v_or0006_MC_D1_110 : STD_LOGIC; 
  signal counteri_carry_v_or0006_MC_D2_111 : STD_LOGIC; 
  signal cnt_2_MC_UIM_112 : STD_LOGIC; 
  signal cnt_1_MC_UIM_113 : STD_LOGIC; 
  signal cnt_9_MC_UIM_114 : STD_LOGIC; 
  signal cnt_3_MC_UIM_115 : STD_LOGIC; 
  signal cnt_5_MC_UIM_116 : STD_LOGIC; 
  signal cnt_4_MC_UIM_117 : STD_LOGIC; 
  signal cnt_7_MC_UIM_118 : STD_LOGIC; 
  signal cnt_6_MC_UIM_119 : STD_LOGIC; 
  signal cnt_8_MC_UIM_120 : STD_LOGIC; 
  signal counteri_carry_v_or0006_MC_D2_PT_0_121 : STD_LOGIC; 
  signal counteri_carry_v_or0006_MC_D2_PT_1_122 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_124 : STD_LOGIC; 
  signal cnt_2_MC_tsimcreated_xor_Q_125 : STD_LOGIC; 
  signal cnt_2_MC_D1_126 : STD_LOGIC; 
  signal cnt_2_MC_D2_127 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_128 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_130 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_131 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_132 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_133 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_135 : STD_LOGIC; 
  signal cnt_1_MC_tsimcreated_xor_Q_136 : STD_LOGIC; 
  signal cnt_1_MC_D1_137 : STD_LOGIC; 
  signal cnt_1_MC_D2_138 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_139 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_141 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_142 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_3_143 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_4_144 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D_146 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D1_147 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D2_148 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D_150 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D1_151 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D2_152 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_154 : STD_LOGIC; 
  signal cnt_9_MC_tsimcreated_xor_Q_155 : STD_LOGIC; 
  signal cnt_9_MC_D1_156 : STD_LOGIC; 
  signal cnt_9_MC_D2_157 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_158 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_160 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_161 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_3_162 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_4_163 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_165 : STD_LOGIC; 
  signal cnt_3_MC_tsimcreated_xor_Q_166 : STD_LOGIC; 
  signal cnt_3_MC_D1_167 : STD_LOGIC; 
  signal cnt_3_MC_D2_168 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_169 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_171 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_2_172 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_3_173 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_4_174 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D_176 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D1_177 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D2_178 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_180 : STD_LOGIC; 
  signal cnt_5_MC_tsimcreated_xor_Q_181 : STD_LOGIC; 
  signal cnt_5_MC_D1_182 : STD_LOGIC; 
  signal cnt_5_MC_D2_183 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_184 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_186 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_187 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_3_188 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_4_189 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_191 : STD_LOGIC; 
  signal cnt_4_MC_tsimcreated_xor_Q_192 : STD_LOGIC; 
  signal cnt_4_MC_D1_193 : STD_LOGIC; 
  signal cnt_4_MC_D2_194 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_195 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_197 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_2_198 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_3_199 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_4_200 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D_202 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D1_203 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D2_204 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D_206 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D1_207 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D2_208 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_7_MC_D_210 : STD_LOGIC; 
  signal cnt_7_MC_tsimcreated_xor_Q_211 : STD_LOGIC; 
  signal cnt_7_MC_D1_212 : STD_LOGIC; 
  signal cnt_7_MC_D2_213 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_0_214 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_1_216 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_2_217 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_3_218 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_4_219 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_221 : STD_LOGIC; 
  signal cnt_6_MC_tsimcreated_xor_Q_222 : STD_LOGIC; 
  signal cnt_6_MC_D1_223 : STD_LOGIC; 
  signal cnt_6_MC_D2_224 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_225 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_227 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_228 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_229 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_230 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D_232 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D1_233 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D2_234 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D_236 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D1_237 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D2_238 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_8_MC_D_240 : STD_LOGIC; 
  signal cnt_8_MC_tsimcreated_xor_Q_241 : STD_LOGIC; 
  signal cnt_8_MC_D1_242 : STD_LOGIC; 
  signal cnt_8_MC_D2_243 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_0_244 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_1_246 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_2_247 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_3_248 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_4_249 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D_251 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D1_252 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D2_253 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D_255 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D1_256 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D2_257 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D_259 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D1_260 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D2_261 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_UIM_263 : STD_LOGIC; 
  signal cnt_11_MC_D_264 : STD_LOGIC; 
  signal cnt_11_MC_tsimcreated_xor_Q_265 : STD_LOGIC; 
  signal cnt_11_MC_D1_266 : STD_LOGIC; 
  signal cnt_11_MC_D2_267 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_268 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_270 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_271 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_272 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_273 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_5_274 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D_276 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D1_277 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D2_278 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_279 : STD_LOGIC; 
  signal did_0_MC_D_280 : STD_LOGIC; 
  signal did_0_MC_D1_281 : STD_LOGIC; 
  signal did_0_MC_D2_282 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_283 : STD_LOGIC; 
  signal did_1_MC_D_284 : STD_LOGIC; 
  signal did_1_MC_D1_285 : STD_LOGIC; 
  signal did_1_MC_D2_286 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_287 : STD_LOGIC; 
  signal did_2_MC_D_288 : STD_LOGIC; 
  signal did_2_MC_D1_289 : STD_LOGIC; 
  signal did_2_MC_D2_290 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_UIM_292 : STD_LOGIC; 
  signal err_MC_D_293 : STD_LOGIC; 
  signal err_MC_D1_294 : STD_LOGIC; 
  signal err_MC_D2_295 : STD_LOGIC; 
  signal err_MC_D2_PT_0_296 : STD_LOGIC; 
  signal err_MC_D2_PT_1_297 : STD_LOGIC; 
  signal err_MC_D2_PT_2_298 : STD_LOGIC; 
  signal err_MC_D2_PT_3_299 : STD_LOGIC; 
  signal err_MC_D2_PT_4_300 : STD_LOGIC; 
  signal err_MC_D2_PT_5_301 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_carry_v_or0006_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal counteri_valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal counteri_updown_cs : STD_LOGIC_VECTOR ( 1 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_5
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_7
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_9
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_11
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_13
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_15
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_17
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_19
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_21
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_23
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_25
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_27
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_29
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_UIM : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_UIM_67
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_69
    );
  Vcc : X_ONE
    port map (
      O => Vcc_70
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_68
    );
  cnt_0_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D1_IN2,
      O => cnt_0_MC_D1_71
    );
  cnt_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      O => cnt_0_MC_D2_PT_0_75
    );
  cnt_0_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN3,
      O => cnt_0_MC_D2_PT_1_78
    );
  cnt_0_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3,
      O => cnt_0_MC_D2_PT_2_79
    );
  cnt_0_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_IN2,
      O => cnt_0_MC_D2_72
    );
  counteri_nLd_cs : X_BUF
    port map (
      I => counteri_nLd_cs_MC_Q,
      O => counteri_nLd_cs_73
    );
  counteri_nLd_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_nLd_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_nLd_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_nLd_cs_MC_Q
    );
  counteri_nLd_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_nLd_cs_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_nLd_cs_MC_D_IN1,
      O => counteri_nLd_cs_MC_D_81
    );
  counteri_nLd_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN1,
      O => counteri_nLd_cs_MC_D1_82
    );
  counteri_nLd_cs_MC_D2 : X_ZERO
    port map (
      O => counteri_nLd_cs_MC_D2_83
    );
  counteri_updown_cs_0_Q : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_Q,
      O => counteri_updown_cs(0)
    );
  counteri_updown_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_updown_cs_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_updown_cs_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_updown_cs_0_MC_Q
    );
  counteri_updown_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN1,
      O => counteri_updown_cs_0_MC_D_85
    );
  counteri_updown_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN1,
      O => counteri_updown_cs_0_MC_D1_86
    );
  counteri_updown_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_updown_cs_0_MC_D2_87
    );
  counteri_updown_cs_1_Q : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_Q,
      O => counteri_updown_cs(1)
    );
  counteri_updown_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_updown_cs_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_updown_cs_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_updown_cs_1_MC_Q
    );
  counteri_updown_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN1,
      O => counteri_updown_cs_1_MC_D_89
    );
  counteri_updown_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN1,
      O => counteri_updown_cs_1_MC_D1_90
    );
  counteri_updown_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_updown_cs_1_MC_D2_91
    );
  counteri_valLd_cs_0_Q : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_Q,
      O => counteri_valLd_cs(0)
    );
  counteri_valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_0_MC_Q
    );
  counteri_valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1,
      O => counteri_valLd_cs_0_MC_D_93
    );
  counteri_valLd_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1,
      O => counteri_valLd_cs_0_MC_D1_94
    );
  counteri_valLd_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_0_MC_D2_95
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_UIM : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_UIM_97
    );
  cnt_10_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN1,
      O => cnt_10_MC_tsimcreated_xor_Q_99
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_98
    );
  cnt_10_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D1_IN2,
      O => cnt_10_MC_D1_100
    );
  cnt_10_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      O => cnt_10_MC_D2_PT_0_103
    );
  cnt_10_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2,
      O => cnt_10_MC_D2_PT_1_105
    );
  cnt_10_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3,
      O => cnt_10_MC_D2_PT_2_106
    );
  cnt_10_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3,
      O => cnt_10_MC_D2_PT_3_107
    );
  cnt_10_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_IN3,
      O => cnt_10_MC_D2_101
    );
  counteri_carry_v_or0006 : X_BUF
    port map (
      I => counteri_carry_v_or0006_MC_Q_108,
      O => counteri_carry_v_or0006_102
    );
  counteri_carry_v_or0006_MC_Q : X_BUF
    port map (
      I => counteri_carry_v_or0006_MC_D_109,
      O => counteri_carry_v_or0006_MC_Q_108
    );
  counteri_carry_v_or0006_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_carry_v_or0006_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_carry_v_or0006_MC_D_IN1,
      O => counteri_carry_v_or0006_MC_D_109
    );
  counteri_carry_v_or0006_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_carry_v_or0006_MC_D1_IN0,
      I1 => NlwInverterSignal_counteri_carry_v_or0006_MC_D1_IN1,
      O => counteri_carry_v_or0006_MC_D1_110
    );
  counteri_carry_v_or0006_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN4,
      I5 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN5,
      I6 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN6,
      I7 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN7,
      I8 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN8,
      I9 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN9,
      I10 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN15,
      O => counteri_carry_v_or0006_MC_D2_PT_0_121
    );
  counteri_carry_v_or0006_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN6,
      I7 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN7,
      I8 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN8,
      I9 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN9,
      I10 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN10,
      I11 => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN15,
      O => counteri_carry_v_or0006_MC_D2_PT_1_122
    );
  counteri_carry_v_or0006_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_IN0,
      I1 => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_IN1,
      O => counteri_carry_v_or0006_MC_D2_111
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_UIM : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_UIM_112
    );
  cnt_2_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1,
      O => cnt_2_MC_tsimcreated_xor_Q_125
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_124
    );
  cnt_2_MC_D1 : X_ZERO
    port map (
      O => cnt_2_MC_D1_126
    );
  cnt_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      O => cnt_2_MC_D2_PT_0_128
    );
  cnt_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      O => cnt_2_MC_D2_PT_1_130
    );
  cnt_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => cnt_2_MC_D2_PT_2_131
    );
  cnt_2_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN5,
      O => cnt_2_MC_D2_PT_3_132
    );
  cnt_2_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN5,
      O => cnt_2_MC_D2_PT_4_133
    );
  cnt_2_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      O => cnt_2_MC_D2_127
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_UIM : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_UIM_113
    );
  cnt_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_MC_tsimcreated_xor_Q_136
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_135
    );
  cnt_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_MC_D1_137
    );
  cnt_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      O => cnt_1_MC_D2_PT_0_139
    );
  cnt_1_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2,
      O => cnt_1_MC_D2_PT_1_141
    );
  cnt_1_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3,
      O => cnt_1_MC_D2_PT_2_142
    );
  cnt_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4,
      O => cnt_1_MC_D2_PT_3_143
    );
  cnt_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4,
      O => cnt_1_MC_D2_PT_4_144
    );
  cnt_1_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_IN4,
      O => cnt_1_MC_D2_138
    );
  counteri_valLd_cs_1_Q : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_Q,
      O => counteri_valLd_cs(1)
    );
  counteri_valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_1_MC_Q
    );
  counteri_valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1,
      O => counteri_valLd_cs_1_MC_D_146
    );
  counteri_valLd_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1,
      O => counteri_valLd_cs_1_MC_D1_147
    );
  counteri_valLd_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_1_MC_D2_148
    );
  counteri_valLd_cs_2_Q : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_Q,
      O => counteri_valLd_cs(2)
    );
  counteri_valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_2_MC_Q
    );
  counteri_valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1,
      O => counteri_valLd_cs_2_MC_D_150
    );
  counteri_valLd_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1,
      O => counteri_valLd_cs_2_MC_D1_151
    );
  counteri_valLd_cs_2_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_2_MC_D2_152
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_UIM : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_UIM_114
    );
  cnt_9_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1,
      O => cnt_9_MC_tsimcreated_xor_Q_155
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_154
    );
  cnt_9_MC_D1 : X_ZERO
    port map (
      O => cnt_9_MC_D1_156
    );
  cnt_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      O => cnt_9_MC_D2_PT_0_158
    );
  cnt_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2,
      O => cnt_9_MC_D2_PT_1_160
    );
  cnt_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      O => cnt_9_MC_D2_PT_2_161
    );
  cnt_9_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15,
      O => cnt_9_MC_D2_PT_3_162
    );
  cnt_9_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN9,
      I10 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN10,
      I11 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN11,
      I12 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN15,
      O => cnt_9_MC_D2_PT_4_163
    );
  cnt_9_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_IN4,
      O => cnt_9_MC_D2_157
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_UIM : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_UIM_115
    );
  cnt_3_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1,
      O => cnt_3_MC_tsimcreated_xor_Q_166
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_165
    );
  cnt_3_MC_D1 : X_ZERO
    port map (
      O => cnt_3_MC_D1_167
    );
  cnt_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      O => cnt_3_MC_D2_PT_0_169
    );
  cnt_3_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2,
      O => cnt_3_MC_D2_PT_1_171
    );
  cnt_3_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3,
      O => cnt_3_MC_D2_PT_2_172
    );
  cnt_3_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6,
      O => cnt_3_MC_D2_PT_3_173
    );
  cnt_3_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN6,
      O => cnt_3_MC_D2_PT_4_174
    );
  cnt_3_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_IN4,
      O => cnt_3_MC_D2_168
    );
  counteri_valLd_cs_3_Q : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_Q,
      O => counteri_valLd_cs(3)
    );
  counteri_valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_3_MC_Q
    );
  counteri_valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1,
      O => counteri_valLd_cs_3_MC_D_176
    );
  counteri_valLd_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1,
      O => counteri_valLd_cs_3_MC_D1_177
    );
  counteri_valLd_cs_3_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_3_MC_D2_178
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_UIM : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_UIM_116
    );
  cnt_5_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1,
      O => cnt_5_MC_tsimcreated_xor_Q_181
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_180
    );
  cnt_5_MC_D1 : X_ZERO
    port map (
      O => cnt_5_MC_D1_182
    );
  cnt_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      O => cnt_5_MC_D2_PT_0_184
    );
  cnt_5_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2,
      O => cnt_5_MC_D2_PT_1_186
    );
  cnt_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      O => cnt_5_MC_D2_PT_2_187
    );
  cnt_5_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN15,
      O => cnt_5_MC_D2_PT_3_188
    );
  cnt_5_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15,
      O => cnt_5_MC_D2_PT_4_189
    );
  cnt_5_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_IN4,
      O => cnt_5_MC_D2_183
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_UIM : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_UIM_117
    );
  cnt_4_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1,
      O => cnt_4_MC_tsimcreated_xor_Q_192
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_191
    );
  cnt_4_MC_D1 : X_ZERO
    port map (
      O => cnt_4_MC_D1_193
    );
  cnt_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      O => cnt_4_MC_D2_PT_0_195
    );
  cnt_4_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2,
      O => cnt_4_MC_D2_PT_1_197
    );
  cnt_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3,
      O => cnt_4_MC_D2_PT_2_198
    );
  cnt_4_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7,
      O => cnt_4_MC_D2_PT_3_199
    );
  cnt_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7,
      O => cnt_4_MC_D2_PT_4_200
    );
  cnt_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_IN4,
      O => cnt_4_MC_D2_194
    );
  counteri_valLd_cs_4_Q : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_Q,
      O => counteri_valLd_cs(4)
    );
  counteri_valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_4_MC_Q
    );
  counteri_valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1,
      O => counteri_valLd_cs_4_MC_D_202
    );
  counteri_valLd_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1,
      O => counteri_valLd_cs_4_MC_D1_203
    );
  counteri_valLd_cs_4_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_4_MC_D2_204
    );
  counteri_valLd_cs_5_Q : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_Q,
      O => counteri_valLd_cs(5)
    );
  counteri_valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_5_MC_Q
    );
  counteri_valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1,
      O => counteri_valLd_cs_5_MC_D_206
    );
  counteri_valLd_cs_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1,
      O => counteri_valLd_cs_5_MC_D1_207
    );
  counteri_valLd_cs_5_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_5_MC_D2_208
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_UIM : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_UIM_118
    );
  cnt_7_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1,
      O => cnt_7_MC_tsimcreated_xor_Q_211
    );
  cnt_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_7_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_7_MC_Q_tsimrenamed_net_Q
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_210
    );
  cnt_7_MC_D1 : X_ZERO
    port map (
      O => cnt_7_MC_D1_212
    );
  cnt_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1,
      O => cnt_7_MC_D2_PT_0_214
    );
  cnt_7_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2,
      O => cnt_7_MC_D2_PT_1_216
    );
  cnt_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3,
      O => cnt_7_MC_D2_PT_2_217
    );
  cnt_7_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN15,
      O => cnt_7_MC_D2_PT_3_218
    );
  cnt_7_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN9,
      I10 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN15,
      O => cnt_7_MC_D2_PT_4_219
    );
  cnt_7_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_IN4,
      O => cnt_7_MC_D2_213
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_UIM : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_UIM_119
    );
  cnt_6_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1,
      O => cnt_6_MC_tsimcreated_xor_Q_222
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_221
    );
  cnt_6_MC_D1 : X_ZERO
    port map (
      O => cnt_6_MC_D1_223
    );
  cnt_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      O => cnt_6_MC_D2_PT_0_225
    );
  cnt_6_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2,
      O => cnt_6_MC_D2_PT_1_227
    );
  cnt_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      O => cnt_6_MC_D2_PT_2_228
    );
  cnt_6_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN15,
      O => cnt_6_MC_D2_PT_3_229
    );
  cnt_6_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15,
      O => cnt_6_MC_D2_PT_4_230
    );
  cnt_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      O => cnt_6_MC_D2_224
    );
  counteri_valLd_cs_6_Q : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_Q,
      O => counteri_valLd_cs(6)
    );
  counteri_valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_6_MC_Q
    );
  counteri_valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1,
      O => counteri_valLd_cs_6_MC_D_232
    );
  counteri_valLd_cs_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1,
      O => counteri_valLd_cs_6_MC_D1_233
    );
  counteri_valLd_cs_6_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_6_MC_D2_234
    );
  counteri_valLd_cs_7_Q : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_Q,
      O => counteri_valLd_cs(7)
    );
  counteri_valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_7_MC_Q
    );
  counteri_valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1,
      O => counteri_valLd_cs_7_MC_D_236
    );
  counteri_valLd_cs_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1,
      O => counteri_valLd_cs_7_MC_D1_237
    );
  counteri_valLd_cs_7_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_7_MC_D2_238
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_UIM : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_UIM_120
    );
  cnt_8_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1,
      O => cnt_8_MC_tsimcreated_xor_Q_241
    );
  cnt_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_8_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_8_MC_Q_tsimrenamed_net_Q
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_240
    );
  cnt_8_MC_D1 : X_ZERO
    port map (
      O => cnt_8_MC_D1_242
    );
  cnt_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1,
      O => cnt_8_MC_D2_PT_0_244
    );
  cnt_8_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN2,
      O => cnt_8_MC_D2_PT_1_246
    );
  cnt_8_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3,
      O => cnt_8_MC_D2_PT_2_247
    );
  cnt_8_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN15,
      O => cnt_8_MC_D2_PT_3_248
    );
  cnt_8_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN7,
      I8 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN8,
      I9 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN9,
      I10 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN10,
      I11 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN15,
      O => cnt_8_MC_D2_PT_4_249
    );
  cnt_8_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_IN4,
      O => cnt_8_MC_D2_243
    );
  counteri_valLd_cs_8_Q : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_Q,
      O => counteri_valLd_cs(8)
    );
  counteri_valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_8_MC_Q
    );
  counteri_valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1,
      O => counteri_valLd_cs_8_MC_D_251
    );
  counteri_valLd_cs_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1,
      O => counteri_valLd_cs_8_MC_D1_252
    );
  counteri_valLd_cs_8_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_8_MC_D2_253
    );
  counteri_valLd_cs_9_Q : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_Q,
      O => counteri_valLd_cs(9)
    );
  counteri_valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_9_MC_Q
    );
  counteri_valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1,
      O => counteri_valLd_cs_9_MC_D_255
    );
  counteri_valLd_cs_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1,
      O => counteri_valLd_cs_9_MC_D1_256
    );
  counteri_valLd_cs_9_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_9_MC_D2_257
    );
  counteri_valLd_cs_10_Q : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_Q,
      O => counteri_valLd_cs(10)
    );
  counteri_valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_10_MC_Q
    );
  counteri_valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1,
      O => counteri_valLd_cs_10_MC_D_259
    );
  counteri_valLd_cs_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1,
      O => counteri_valLd_cs_10_MC_D1_260
    );
  counteri_valLd_cs_10_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_10_MC_D2_261
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_UIM : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_UIM_263
    );
  cnt_11_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1,
      O => cnt_11_MC_tsimcreated_xor_Q_265
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_264
    );
  cnt_11_MC_D1 : X_ZERO
    port map (
      O => cnt_11_MC_D1_266
    );
  cnt_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => cnt_11_MC_D2_PT_0_268
    );
  cnt_11_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2,
      O => cnt_11_MC_D2_PT_1_270
    );
  cnt_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => cnt_11_MC_D2_PT_2_271
    );
  cnt_11_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4,
      O => cnt_11_MC_D2_PT_3_272
    );
  cnt_11_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4,
      O => cnt_11_MC_D2_PT_4_273
    );
  cnt_11_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN5,
      O => cnt_11_MC_D2_PT_5_274
    );
  cnt_11_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_IN5,
      O => cnt_11_MC_D2_267
    );
  counteri_valLd_cs_11_Q : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_Q,
      O => counteri_valLd_cs(11)
    );
  counteri_valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => counteri_valLd_cs_11_MC_Q
    );
  counteri_valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1,
      O => counteri_valLd_cs_11_MC_D_276
    );
  counteri_valLd_cs_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1,
      O => counteri_valLd_cs_11_MC_D1_277
    );
  counteri_valLd_cs_11_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_11_MC_D2_278
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_279,
      O => did_0_MC_Q_59
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_280,
      O => did_0_MC_Q_tsimrenamed_net_Q_279
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_280
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_281
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_282
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_283,
      O => did_1_MC_Q_61
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_284,
      O => did_1_MC_Q_tsimrenamed_net_Q_283
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_284
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_285
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_286
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_287,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_288,
      O => did_2_MC_Q_tsimrenamed_net_Q_287
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_288
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_289
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_290
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_UIM : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_UIM_292
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_293
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_294
    );
  err_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_0_IN1,
      O => err_MC_D2_PT_0_296
    );
  err_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_1_IN1,
      O => err_MC_D2_PT_1_297
    );
  err_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_2_IN2,
      O => err_MC_D2_PT_2_298
    );
  err_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_3_IN2,
      O => err_MC_D2_PT_3_299
    );
  err_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_4_IN3,
      O => err_MC_D2_PT_4_300
    );
  err_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_5_IN4,
      O => err_MC_D2_PT_5_301
    );
  err_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      I4 => NlwBufferSignal_err_MC_D2_IN4,
      I5 => NlwBufferSignal_err_MC_D2_IN5,
      O => err_MC_D2_295
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_68,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_72,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_75,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_78,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_2_79,
      O => NlwBufferSignal_cnt_0_MC_D2_IN2
    );
  NlwBufferBlock_counteri_nLd_cs_MC_REG_IN : X_BUF
    port map (
      I => counteri_nLd_cs_MC_D_81,
      O => NlwBufferSignal_counteri_nLd_cs_MC_REG_IN
    );
  NlwBufferBlock_counteri_nLd_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_nLd_cs_MC_REG_CLK
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_MC_D1_82,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D_IN0
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_MC_D2_83,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D_IN1
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN0
    );
  NlwBufferBlock_counteri_nLd_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_5,
      O => NlwBufferSignal_counteri_nLd_cs_MC_D1_IN1
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_D_85,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_D1_86,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_updown_cs_0_MC_D2_87,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_updown_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_7,
      O => NlwBufferSignal_counteri_updown_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_D_89,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_D1_90,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_updown_cs_1_MC_D2_91,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_updown_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_9,
      O => NlwBufferSignal_counteri_updown_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D_93,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D1_94,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D2_95,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_0_II_UIM_11,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D_98,
      O => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_10_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_tsimcreated_xor_Q_99,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_100,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_101,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN2 : X_BUF
    port map (
      I => counteri_carry_v_or0006_102,
      O => NlwBufferSignal_cnt_10_MC_D1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_103,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_105,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_2_106,
      O => NlwBufferSignal_cnt_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_3_107,
      O => NlwBufferSignal_cnt_10_MC_D2_IN3
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D_IN0 : X_BUF
    port map (
      I => counteri_carry_v_or0006_MC_D1_110,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D_IN0
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D_IN1 : X_BUF
    port map (
      I => counteri_carry_v_or0006_MC_D2_111,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D_IN1
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D1_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D1_IN0
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D1_IN1
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_9_MC_UIM_114,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_9_MC_UIM_114,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_IN0 : X_BUF
    port map (
      I => counteri_carry_v_or0006_MC_D2_PT_0_121,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_IN0
    );
  NlwBufferBlock_counteri_carry_v_or0006_MC_D2_IN1 : X_BUF
    port map (
      I => counteri_carry_v_or0006_MC_D2_PT_1_122,
      O => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D_124,
      O => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_2_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_tsimcreated_xor_Q_125,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_126,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_127,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_128,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_130,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_131,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_132,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_133,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D_135,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_tsimcreated_xor_Q_136,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_137,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_138,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_139,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_141,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_142,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_3_143,
      O => NlwBufferSignal_cnt_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_4_144,
      O => NlwBufferSignal_cnt_1_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D_146,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D1_147,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D2_148,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_1_II_UIM_13,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D_150,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D1_151,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D2_152,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_2_II_UIM_15,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D_154,
      O => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_9_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_tsimcreated_xor_Q_155,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_156,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_157,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_114,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_114,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_UIM_114,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_158,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_160,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_161,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_3_162,
      O => NlwBufferSignal_cnt_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_4_163,
      O => NlwBufferSignal_cnt_9_MC_D2_IN4
    );
  NlwBufferBlock_cnt_3_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D_165,
      O => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_3_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_tsimcreated_xor_Q_166,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_167,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_168,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_169,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_171,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_2_172,
      O => NlwBufferSignal_cnt_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_3_173,
      O => NlwBufferSignal_cnt_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_4_174,
      O => NlwBufferSignal_cnt_3_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D_176,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D1_177,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D2_178,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_3_II_UIM_17,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D_180,
      O => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_5_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_tsimcreated_xor_Q_181,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_182,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_183,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_184,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_186,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_187,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_3_188,
      O => NlwBufferSignal_cnt_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_4_189,
      O => NlwBufferSignal_cnt_5_MC_D2_IN4
    );
  NlwBufferBlock_cnt_4_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D_191,
      O => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_4_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_tsimcreated_xor_Q_192,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_193,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_194,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_195,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_197,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_2_198,
      O => NlwBufferSignal_cnt_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_3_199,
      O => NlwBufferSignal_cnt_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_4_200,
      O => NlwBufferSignal_cnt_4_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D_202,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D1_203,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D2_204,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_4_II_UIM_19,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D_206,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D1_207,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D2_208,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_5_II_UIM_21,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D_210,
      O => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_7_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_MC_tsimcreated_xor_Q_211,
      O => NlwBufferSignal_cnt_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_212,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_213,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_0_214,
      O => NlwBufferSignal_cnt_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_1_216,
      O => NlwBufferSignal_cnt_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_2_217,
      O => NlwBufferSignal_cnt_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_3_218,
      O => NlwBufferSignal_cnt_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_4_219,
      O => NlwBufferSignal_cnt_7_MC_D2_IN4
    );
  NlwBufferBlock_cnt_6_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D_221,
      O => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_6_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_tsimcreated_xor_Q_222,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_223,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_224,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_225,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_227,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_228,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_229,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_230,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D_232,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D1_233,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D2_234,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_6_II_UIM_23,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D_236,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D1_237,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D2_238,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_7_II_UIM_25,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D_240,
      O => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_8_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_MC_tsimcreated_xor_Q_241,
      O => NlwBufferSignal_cnt_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_242,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_243,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_8_MC_UIM_120,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_112,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_MC_UIM_113,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_115,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_116,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_4_MC_UIM_117,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_7_MC_UIM_118,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_6_MC_UIM_119,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_0_244,
      O => NlwBufferSignal_cnt_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_1_246,
      O => NlwBufferSignal_cnt_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_2_247,
      O => NlwBufferSignal_cnt_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_3_248,
      O => NlwBufferSignal_cnt_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_4_249,
      O => NlwBufferSignal_cnt_8_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D_251,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D1_252,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D2_253,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_27,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D_255,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D1_256,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D2_257,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_9_II_UIM_29,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D_259,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D1_260,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D2_261,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_10_II_UIM_31,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D_264,
      O => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_11_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_tsimcreated_xor_Q_265,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_266,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_267,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_263,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_263,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_UIM_263,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_carry_v_or0006_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_carry_v_or0006_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => counteri_carry_v_or0006_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_268,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_270,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_271,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_272,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_273,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_5_274,
      O => NlwBufferSignal_cnt_11_MC_D2_IN5
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D_276,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D1_277,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D2_278,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_281,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_282,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_285,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_286,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_289,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_290,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_293,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_294,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_295,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => counteri_carry_v_or0006_102,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_263,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => err_MC_UIM_292,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_263,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => err_MC_UIM_292,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_11_MC_UIM_263,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => err_MC_UIM_292,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => counteri_updown_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => counteri_updown_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => counteri_carry_v_or0006_102,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_97,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => err_MC_UIM_292,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_296,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_297,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_298,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_299,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwBufferBlock_err_MC_D2_IN4 : X_BUF
    port map (
      I => err_MC_D2_PT_4_300,
      O => NlwBufferSignal_err_MC_D2_IN4
    );
  NlwBufferBlock_err_MC_D2_IN5 : X_BUF
    port map (
      I => err_MC_D2_PT_5_301,
      O => NlwBufferSignal_err_MC_D2_IN5
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D1_IN1,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D1_IN1
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN7 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN7,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN7
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN8 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN8,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN8
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN9 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN9,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN9
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN10 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN10,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN10
    );
  NlwInverterBlock_counteri_carry_v_or0006_MC_D2_PT_1_IN11 : X_INV
    port map (
      I => NlwBufferSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN11,
      O => NlwInverterSignal_counteri_carry_v_or0006_MC_D2_PT_1_IN11
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN10,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN10
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN11,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN11
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN12,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN12
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN10,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN10
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN8,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN8
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN9,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN9
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN10,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN10
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN11,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN11
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_did_0_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_did_0_MC_D_IN0,
      O => NlwInverterSignal_did_0_MC_D_IN0
    );
  NlwInverterBlock_err_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D_IN0,
      O => NlwInverterSignal_err_MC_D_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN4
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

