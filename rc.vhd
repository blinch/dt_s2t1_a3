-- response checker for 11bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A3


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
    
entity responseCheck is 
    port(
        count : in std_logic_vector(11 downto 0);
        did   : in std_logic_vector(2 downto 0);
        err   : in std_logic
    );
end entity responseCheck;


architecture arc of responseCheck is 
    signal ok_s : boolean := true; -- boolean, nicht std_logic!
begin
    
    combiLogic:
    process is
    begin

        -- 1clock is 20ns,see constant fullClockCycle in stimuli generator
        -- wait for 2clocks of reset and 10 clocks of cpld wakeup
        wait for 12 * 20 ns;

        -- ok_s <= true|false
        -- ok_s <= condition of count-Value and error value
        -- example:
        ok_s <= (count = "000000000000" and err = '0');

        -- up count test from 0 to 5
        wait for 6 * 20 ns;
        ok_s <= (count = "000000000101" and err = '0');
        
        -- reset
        wait for 2 * 20 ns;
        ok_s <= (count = "000000000000" and err = '0');

        --load value test, loaded value is 16
        wait for 2 * 20 ns;
        ok_s <= (count = "000000010000" and err = '0');

        --down count test from 16 to 6
        wait for 12 * 20 ns;
        ok_s <= (count = "000000000110" and err = '0');

        --load value test, loaded value is 2^12, max value
        wait for 3 * 20 ns;
        ok_s <= (count = "111111111111" and err = '0');

        -- overflow test
        wait for 6 * 20 ns;
        ok_s <= (count = "000000000000" and err = '1');

        -- reset
        wait for 2 * 20 ns;
        ok_s <= (count = "000000000000" and err = '0');

        --underflow test
        wait for 4 * 20 ns;
        ok_s <= (count = "111111111111" and err = '1');

        wait;
    end process combiLogic;
end architecture arc;